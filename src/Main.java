import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static String FILE = "D:/Training";//Point to user
    private static String FILE1 = "D:/Training/test";//Point to user
    private static String FILE2 = "D:/Training/Project/companies_big_data.csv";//Point to user

    public static void main(String[] args) throws IOException {
        HandleFile handleFile = new HandleFile();
        String fileName = "companies.csv";

        //get path of file
        Path path = Paths.get(FILE + "/Project/" + fileName);

        //Print many types of path
        System.out.println("Absolute path: " + path.toAbsolutePath());
        System.out.println("URI: " + path.toUri());
        System.out.println("Normalized Path: " + path.normalize());
        System.out.println("Normalized URI: " + path.normalize().toUri());
        System.out.println();

        //Readfile
        List<List<String>> data = handleFile.readCSVFile(path);
        List<Object> result = handleFile.sortFileHadReadByCondition("CH","Capital",data);
        List<Object> result2 = handleFile.sortFileHadReadByCondition("VN","Name",data);
        System.out.println(result);
        System.out.println(result2);

        //Monitor File
        MyFileVisitor myFileVisitor = new MyFileVisitor();
        Path path1 = Paths.get(FILE + "/Project");
        Files.walkFileTree(path1,myFileVisitor);

        //compress file
        File inputDir = new File(FILE2);
        File outputZipFile = new File(FILE + "/Project/data.zip");

        if(handleFile.zipFile(inputDir, outputZipFile)){
            System.out.println("Zipped");
        }else{
            System.out.println("Error ocurs");
        }
    }
}

