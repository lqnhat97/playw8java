import java.awt.*;
import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class HandleFile {

    private final static String COMMA_DELIMITER = ",";

    /**
     * Read CSV File then save it to an Array
     * @param path
     * @return
     */
    public List<List<String>> readCSVFile(Path path){
        List<List<String>> records = new ArrayList<>();

        //Use try-with-resources to read file (Java NIO2)
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path.toString()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));
            }
            return records;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface HandleReadFile{
        List<Object> sort(List<Object> list);
    }

    public static List<Object> sortedList(List<Object> list,HandleReadFile handleReadFile){
        return handleReadFile.sort(list);
    }
    /**
     * Sort data had read by condition
     * @param conditionCap,conditionName
     * @param fileRead
     * @return
     */
    public List<Object> sortFileHadReadByCondition(String conditionCap, String conditionName, List<List<String>> fileRead){
        //Loop using Iterator
            /*Iterator<List<String>> it = fileRead.iterator();
            while (it.hasNext()) {
                List<String> data = it.next();
                System.out.println(data);
            }*/
        int capitalPos = fileRead.get(0).indexOf(conditionName);
        //Create result arr
        List<Object> result = new ArrayList<>();
        //Loop using forEach
        fileRead.forEach(row -> {
            if (row.contains(conditionCap)) {
                result.add(row.get(capitalPos));
            }
        });
        List<Object> sort = sortedList(result,list -> {
            Collections.sort(list, Collections.reverseOrder());
            return list;
        });
        //Sort arr
        return sort;
    }

    /**
     * Zip file
     * @param inputDir
     * @param zipFile
     * @return
     */
    public boolean zipFile(File inputDir, File zipFile){
        zipFile.getParentFile().mkdir();
        String inputDirPath = inputDir.getAbsolutePath();
        byte[] buffer = new byte[1024];
        try (FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
             ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)){
            List<File> files = new ArrayList<>();
            if(inputDir.isFile()){
                files.add(inputDir);
            }else {
                files = listChildFiles(inputDir);
            }
            for (File file:files) {
                String filePath = file.getAbsolutePath();
                System.out.println("Zip:" + filePath);
                String entryName = inputDir.isFile()?filePath.substring(inputDir.getParentFile().getAbsolutePath().length()+1): filePath.substring(inputDirPath.length() + 1);
                ZipEntry ze = new ZipEntry(entryName);
                zipOutputStream.putNextEntry(ze);
                // Đọc dữ liệu của file và ghi vào ZipOutputStream.
                FileInputStream fileIs = new FileInputStream(filePath);

                int len;
                while ((len = fileIs.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private List<File> listChildFiles(File dir){
        List<File> allFiles = new ArrayList<>();

        File[] childFiles = dir.listFiles();
        for (File file : childFiles) {
            if (file.isFile()) {
                allFiles.add(file);
                continue;
            }
            List<File> files = this.listChildFiles(file);
            allFiles.addAll(files);
        }
        return allFiles;
    }
}
